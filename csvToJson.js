const csvtojson = require('csvtojson');

convertedData = function (csvFilePath) {
    return(
        csvtojson()
        .fromFile(csvFilePath)
        .then((jsonObj) => {
            return jsonObj;
        })
    )  
  };

module.exports = convertedData;